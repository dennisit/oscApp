
import Api from "../common/Api"
import Util from "../common/Util"
import Home from "../../home"

import ToolBar from "../common/ToolBar"

var HTMLWebView = require('react-native-html-webview');

var moment=require('moment')
var zhCn=require('moment/locale/zh-cn')

var Dimensions = require('Dimensions');
var React = require('react-native')
var {
  ScrollView,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  WebView
}=React;

export default class NewsDetail extends React.Component{
  constructor(props){
    super(props)
    this.state={
      data:{},
      show:false,
    }
  }

  componentDidMount(){
    var id = this.props.id;
    var url = Api.news_detail+"/"+id;
    console.log(url)
    var that =this;
    Util.get(url,function(data){
      that.setState({
        data:data.oschina.news,
        show:true,
      });
    },function(err){
      alert(err);
    });

  }

  _loadPage(){
    this.props.navigator.push({
      component:Home
    })
  }

  _onLink(href){
    console.log(href)
  }

  render(){
    var data=this.state.data;
    const pubDate=moment(data.pubDate,'YYYY-MM-DD hh:mm:ss').startOf('hour').fromNow();
    //解决HTMLWebView组件图片显示不全问题
    var body = "<style type='text/css'>img { max-width: 100%; height: auto }</style>"+data.body;
    return (
      <View style={{flex:1}}>
        <View style={styles.container}>
        {
          this.state.show && data.title!==undefined?
          <ScrollView >
            <View>
              <Text style={{fontWeight:'bold',fontSize:17}}>{data.title}</Text>
            </View>
            <View style={{flexDirection:'row',marginTop:5,marginBottom:10}}>
              <Text style={{color:'green',fontWeight:'bold',fontSize:13}}>{data.author}</Text>
              <Text style={{color:'gray',fontSize:13,marginLeft:5}}>{pubDate}</Text>
            </View>
            <HTMLWebView
              style={{flex:1}}
              html={body}
              makeSafe={true}
              autoHeight={true}
              onLink={this._onLink}
              />
          </ScrollView>
          : Util.loading
        }

        </View>
        <ToolBar/>
        </View>
    );
  }
}

var styles=StyleSheet.create({
  container:{
    //backgroundColor:'#eeedf1',
    flex:1,
    marginTop:5,
    marginLeft:5,
    marginRight:5,
    flexDirection:'column',
  },

});
