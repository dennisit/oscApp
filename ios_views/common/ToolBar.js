/**
*
*/
'use strict'

import Util from "./Util"

var React = require('react-native');
var {
  View,
  Text,
  StyleSheet,
  Component,
  TouchableOpacity,
  Image,
} =React;

export default class ToolBar extends Component{
  constructor(props){
    super(props)
  }

  render(){
    return (
      <View style={styles.container}>
        <TouchableOpacity>
          <Image source={require('image!toolbar-keyboardUp')}/>
        </TouchableOpacity>
        <TouchableOpacity>
          <Image source={require('image!toolbar-comments')}/>
        </TouchableOpacity>
        <TouchableOpacity>
          <Image source={require('image!toolbar-editingComment')}/>
        </TouchableOpacity>
        <TouchableOpacity>
          <Image source={require('image!toolbar-star')}/>
        </TouchableOpacity>
        <TouchableOpacity>
          <Image source={require('image!toolbar-share')}/>
        </TouchableOpacity>
      </View>
    );
  }
}

var styles=StyleSheet.create({
    container:{
      height:40,
      marginRight:0,
      marginLeft:0,
      backgroundColor:'#f9f4fa',
      borderColor:'#ccc',
      borderWidth:Util.pixel,
      justifyContent:'space-around',
      flexDirection:'row',
      alignItems:'center'
    }
});
