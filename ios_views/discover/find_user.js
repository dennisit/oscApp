/**
* 找人
*/
'use strict'
import Util from "../common/Util"
import Api from "../common/Api"

var React = require('react-native');
var {
  ScrollView,
  View,
  TextInput,
  Text,
  StyleSheet,
  ListView,
  Image,
  TouchableOpacity,
}=React;

export default class FindUser extends React.Component{
  constructor(props){
    super(props)

    this.state={
      users:new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2}).cloneWithRows([]),
      show:false,
    };
  }

  search(text){
    const that=this;
    this.setState({
        users:new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2}).cloneWithRows([]),
        show:false,
    })
    if(text.length>1){
      var url=[Api.find_user,'name='+text].join('?');
      Util.get(url,(data)=>{
        console.log('find_user:'+JSON.stringify(data));
        var objs=data.oschina.users.user;
        var dataList=[];
        if(!Array.isArray(objs)){
          dataList.push(objs);
        }else{
          dataList=objs;
        }
        that.setState({
          users:new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2}).cloneWithRows(dataList),
          show:true,
        })
      });
    }
  }

  _loadDetailPage(){

  }

  render(){
    return (
      <View style={{flex:1}}>
        <View style={styles.searchRow}>
          <TextInput
            autoFocus={true}
            style={styles.searchTextInput}
            onChangeText={this.search.bind(this)}
            placeholder="输入用户昵称"
          />
        </View>
        <ScrollView style={styles.container}>
          {
            this.state.show?
            <ListView
              dataSource={this.state.users}
              renderRow={(row)=>
                <UserItem row={row} onPress={this._loadDetailPage.bind(this,row.uid)} {...this.props}/>
              }
              />
            :<View></View>
          }
        </ScrollView>
      </View>

    );
  }
}

class UserItem extends React.Component{
    constructor(props){
      super(props)
    }

    render(){
      const {row}=this.props;
      return (
        <TouchableOpacity style={styles.item} {...this.props}>

          <View>
            <Image style={styles.avatar}
            source={row.portrait===''?require('image!default-portrait'):{uri:row.portrait}}></Image>
          </View>
          <View style={{flexDirection:'column',marginLeft:8}}>
            <View style={styles.name}>
              <Text style={{color:'green'}}>{row.name}</Text>
            </View>
            <View style={styles.from}>
              <Text>{row.from}</Text>
            </View>

          </View>
        </TouchableOpacity>
      );
    }
}


var styles=StyleSheet.create({
  container:{
    backgroundColor:'#eeedf1',
    flex:1
  },
  searchRow: {
    backgroundColor: '#eeeeee',
    height:45
  },
  searchTextInput: {
    backgroundColor: 'white',
    marginLeft:10,
    marginRight:10,
    marginTop:5,
    marginBottom:5,
    flex:1,
    borderWidth:Util.pixel,
    height:35,
    paddingLeft:8,
    borderRadius:12,
    borderColor:'#ccc'
  },
  avatar:{
    width:50,
    height:50,
    borderRadius:10,
    borderWidth:Util.pixel,
    borderColor:'#ffffff',
    marginLeft:8
  },

  item:{
    flexDirection:'row',
    height:60,
  },
  name:{
    marginTop:5,

  },
  from:{
    marginTop:5,
  }
});
