/**
* 设置页面
*/

'use strict'

import Util from "../common/Util"
import Api from "../common/Api"

import MeApp from "./index"

var React = require('react-native');

var {
  StyleSheet,
  ScrollView,
  View,
  Text,
  TouchableOpacity,
  AsyncStorage,
  AlertIOS,
}=React;


export default class Config extends React.Component{
  constructor(props){
    super(props)
  }

  _logout(){
    const that = this;
    AsyncStorage.removeItem('uid')
      .then(()=>{
        // that.props.navigator.push({
        //   component:<MeApp/>,
        //   title:'我'
        // })
        that.props.navigator.popToTop();
      })
      .catch((err)=>{
        AlertIOS.alert('注销',err);
      })
      .done();
  }

  render(){
    return (
      <ScrollView style={styles.container}>
          <View style={{marginTop:10}}>
            <TouchableOpacity >
              <View style={styles.item}>
                <Text style={styles.title}>清除缓存</Text>
              </View>
            </TouchableOpacity>
          </View>

          <View style={{marginTop:30}}>
            <TouchableOpacity >
              <View style={styles.item}>
                <Text style={styles.title}>意见反馈</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View >
            <TouchableOpacity >
              <View style={styles.item}>
                <Text style={styles.title}>给应用评分</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity >
              <View style={styles.item}>
                <Text style={styles.title}>关于</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View >
            <TouchableOpacity >
              <View style={styles.item}>
                <Text style={styles.title}>开源许可</Text>
              </View>
            </TouchableOpacity>
          </View>

          <View style={{marginTop:30}}>
            <TouchableOpacity
              onPress={this._logout.bind(this)}>
              <View style={styles.item}>
                <Text style={styles.title}>注销登录</Text>
              </View>
            </TouchableOpacity>
          </View>

      </ScrollView>
    );
  }
}

var styles = StyleSheet.create({
  container:{
    backgroundColor:'#eeedf1'
  },
  item:{
    flexDirection:'column',
    flex:1,
    height:40,
    justifyContent: 'center',
    borderTopWidth: Util.pixel,
    borderTopColor: '#ddd',
    backgroundColor:'#fff',
  },
  title:{
    marginLeft:15,
    marginTop:5,
  },
});
